#!/usr/bin/env zsh

function echo() {
    #builtin echo `color magenta "(Screenshot)"; color white "$@"`
    builtin echo "Screenshot $@"
}

function notify() {
    notify-send --urgency=$1 --expire-time=10000 "SCREENSHOT $2" "$3"
}

function notify_exit() {
    notify $@
    exit 1
}

quiet=false
if [[ $1 == -q ]]; then
    quiet=true
fi

output_file=/tmp/$RANDOM.png
maim --select $output_file || notify_exit critical FAILED "Failed to take screenshot!"

#file_name=$(md5sum $output_file | cut -c-10).png
#img_url=http://i.redrield.com/$file_name
#
#scp -q $output_file images@redrield.com:~/root/$file_name || notify critical FAILED "Failed to upload screenshot!"

echo $output_file

img_url=$(curl -sF "file=@${output_file}" https://0x0.st)


if ! $quiet; then
    echo "Image URL: $img_url"
fi

notify normal UPLOADED $img_url
builtin echo $img_url | xclip -selection clipboard
